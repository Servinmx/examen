<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Login</title>

</head>
<body background="https://image.jimcdn.com/app/cms/image/transf/none/path/s1835fc4dc8c6343c/backgroundarea/i9ff229ed6a2b6856/version/1513185954/image.jpg">
<div class="container">
    <div class=" card">
        <div class="card-header"><center><h5 class="display-5" style="font-family: 'Bodoni MT'" >Cliente-Servidor</h5></center></div>
    </div>

    <div class="form-row" style="margin-top: 20%">

        <div class="col-md-4 offset-2">
            <a class="navbar-brand" href="http://localhost/examen/Index.php?controller=Usuario&action=examen" >
                <img src="https://image.flaticon.com/icons/png/512/410/410832.png" width="110" height="120" style="border-radius: 100px" class="d-inline-block align-top" alt="">

                <h5>Realizar examen </h5>
            </a>
        </div>
        <div class="col-md-4 offset-2">
            <a class="navbar-brand" href="http://localhost/examen/Index.php?controller=Admin&action=login">
                <img src="https://cifpvirgendegracia.com/wp-content/uploads/2019/12/examenes.png" width="120" height="120" class="d-inline-block align-top" alt="">

                <h5>Ver resultados </h5>
            </a>

        </div>


    </div>
    <hr>
</div>
</div>

</body>
<!--JS con Bootstrap-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</html>