<?php
    $conexion=mysqli_connect('localhost', 'root', '', 'examen');
?>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Respuestas</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="public/js/validarNombres.js"></script>
    <script src="public/js/validarFechas.js"></script>
</head>
<body background="https://image.jimcdn.com/app/cms/image/transf/none/path/s1835fc4dc8c6343c/backgroundarea/i9ff229ed6a2b6856/version/1513185954/image.jpg">
<div class="container">
    <h2>Resultado de examen</h2>
    <p>Cliente-Servidor</p>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido Paterno</th>
            <th scope="col">Apellido Materno</th>
            <th scope="col">Correo</th>
            <th scope="col">Fecha</th>
            <th scope="col">Sexo</th>
            <th scope="col">Edad</th>
            <th scope="col">Aciertos</th>
        </tr>
        </thead>
        <tbody>
        <tbody>
        <?php
        $sql="SELECT * FROM `usuarios`";
        $result=mysqli_query($conexion, $sql);
        while($mostrar=mysqli_fetch_array($result)){
            ?>
            <tr>
                <td class="dato"><?php echo $mostrar['id'] ?></td>
                <td class="dato"><?php echo $mostrar['nombre'] ?></td>
                <td class="dato"><?php echo $mostrar['apeidoP'] ?></td>
                <td class="dato"><?php echo $mostrar['apeidoM'] ?></td>
                <td class="dato"><?php echo $mostrar['correo'] ?></td>
                <td class="dato"><?php echo $mostrar['fecha'] ?></td>
                <td class="dato"><?php echo $mostrar['sexo'] ?></td>
                <td class="dato"><?php echo $mostrar['edad'] ?></td>
                <?php
                    $respuestas = null;
                    $respuestas = $mostrar['respuesta1'] + $mostrar['respuesta2'] + $mostrar['respuesta3'] +$mostrar['respuesta4']
                    + $mostrar['respuesta5'] + $mostrar['respuesta6'] + $mostrar['respuesta7'] + $mostrar['respuesta8']
                    + $mostrar['respuesta9'] + $mostrar['respuesta10'];
                ?>
                <td class="dato"><?php echo $respuestas ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <br>
</div>
<br>
<br>
<!--COMPARACION-->
<div class="container " id="comparacion">
    <h2>Comparar respuestas entre 2 Usuarios</h2>
    <form >
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="nombre">Seleccione Nombre</label>
                <select class="form-control" id="nombre1">
                    <?php
                    $sql="SELECT id, nombre FROM `usuarios`";
                    $result=mysqli_query($conexion, $sql);
                    while($mostrar=mysqli_fetch_array($result)){
                        ?>
                        <option value="<?php echo $mostrar['id'] ?>"><?php echo $mostrar['id']."-".$mostrar['nombre'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="apeidoM">Seleccione Nombre</label>
                <select class="form-control" id="nombre2">
                    <?php
                    $sql="SELECT id, nombre FROM `usuarios`";
                    $result=mysqli_query($conexion, $sql);
                    while($mostrar=mysqli_fetch_array($result)){
                        ?>
                        <option value="<?php echo $mostrar['id'] ?>"><?php echo $mostrar['id']."-".$mostrar['nombre'] ?></option>
                        <?php
                    }
                    ?>
                </select>
                <!--Validacion de Telefono-->
                <div id="validarNombres"></div>
            </div>
            <br>
            <br>
            <br>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4 ">
                <a class="btn btn-primary btn-lg" href="#" role="button" id="boton">Comparar</a>
                <!--<button class="btn btn-outline-primary" type="submit" id="comparar">Comparar</button>-->
            </div>
        </div>
    </form>
</div>
<br>
<br>
<br>
<div class="container " id="comparacion">
    <h2>Comparar respuestas entre mujeres y hombres por semana</h2>
    <form >
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="fecha1">Inicio de Semana(Elija un Domingo)</label>
                <input type="date" class="form-control" id="fecha1" name="fecha1" placeholder="Fecha">
                <!--Validacion de Telefono-->
                <div id="validarFecha1"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="fecha2">Inicio de Semana(Elija un Sabado)</label>
                <input type="date" class="form-control" id="fecha2" name="fecha2" placeholder="Fecha">
                <!--Validacion de Telefono-->
                <div id="validarFecha2"></div>
            </div>
            <br>
            <br>
            <br>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4 ">
                <a class="btn btn-primary btn-lg" href="#" role="button" id="buscar">Buscar</a>
                <!--<button class="btn btn-outline-primary" type="submit" id="comparar">Comparar</button>-->
            </div>
        </div>
    </form>
</div>
</body>

</html>

