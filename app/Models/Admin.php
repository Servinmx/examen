<?php


namespace Models;


class Admin extends Conexion
{
    //Funcion para verificar que el Admin este en la base de datos
     static function verificarAdmin($correo, $contrasenia){
        $con = new Conexion();
        $pre = mysqli_prepare($con->con, "SELECT * FROM administradores WHERE correo = ? AND contrasenia = ?");
        $pre->bind_param("ss", $correo, $contrasenia);
        $pre->execute();
        $res = $pre->get_result();
        return $res->fetch_object();
    }
}