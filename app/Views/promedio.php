<?php
$conexion=mysqli_connect('localhost', 'root', '', 'examen');
$f1 = $_GET["f1"];
$f2 = $_GET["f2"];
$mujeres = 0;
$hombres = 0;
$cantmujeres = 0;
$canthombres = 0;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Comparacion</title>
    <link rel="stylesheet" href="public/css/login.css">
</head>
<body >
<div class="container login-container">
    <h2>Resultados de examen</h2>
    <p>Cliente-Servidor</p>
    <p style="color: red">Si alguna tabla aparece vacia es por que no hay registros en la fecha buscada</p>
    <p>Fecha de: <?php echo $f1?></p>
    <p>A Fecha de: <?php echo $f2?></p>
    <br>
    <h3>Mujeres</h3>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido Paterno</th>
            <th scope="col">Apellido Materno</th>
            <th scope="col">Correo</th>
            <th scope="col">Fecha</th>
            <th scope="col">Sexo</th>
            <th scope="col">Edad</th>
            <th scope="col">Aciertos</th>
        </tr>
        </thead>
        <tbody>
        <tbody>
        <?php
        $sql="SELECT * FROM `usuarios` WHERE sexo = 'Femenino' AND (fecha BETWEEN '$f1' AND '$f2')";
        $result=mysqli_query($conexion, $sql);
        while($mostrar=mysqli_fetch_array($result)){
            ?>
        <tr>
            <td class="dato"><?php echo $mostrar['id'] ?></td>
            <td class="dato"><?php echo $mostrar['nombre'] ?></td>
            <td class="dato"><?php echo $mostrar['apeidoP'] ?></td>
            <td class="dato"><?php echo $mostrar['apeidoM'] ?></td>
            <td class="dato"><?php echo $mostrar['correo'] ?></td>
            <td class="dato"><?php echo $mostrar['fecha'] ?></td>
            <td class="dato"><?php echo $mostrar['sexo'] ?></td>
            <td class="dato"><?php echo $mostrar['edad'] ?></td>
            <?php
            $cantmujeres = $cantmujeres + 1;
            $respuestas = 0;
            $respuestas = $mostrar['respuesta1'] + $mostrar['respuesta2'] + $mostrar['respuesta3'] +$mostrar['respuesta4']
                + $mostrar['respuesta5'] + $mostrar['respuesta6'] + $mostrar['respuesta7'] + $mostrar['respuesta8']
                + $mostrar['respuesta9'] + $mostrar['respuesta10'];
            $mujeres = $mujeres + $respuestas;
            ?>
            <td class="dato"><?php echo $respuestas ?></td>
        </tr>
        <?php
        }
        ?>
        </tbody>
        </table>
    <br>
    <h3>Hombres</h3>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido Paterno</th>
            <th scope="col">Apellido Materno</th>
            <th scope="col">Correo</th>
            <th scope="col">Fecha</th>
            <th scope="col">Sexo</th>
            <th scope="col">Edad</th>
            <th scope="col">Aciertos</th>
        </tr>
        </thead>
        <tbody>
        <tbody>
        <?php
        $sql="SELECT * FROM `usuarios` WHERE sexo = 'Masculino' AND (fecha BETWEEN '$f1' AND '$f2')";
        $result=mysqli_query($conexion, $sql);
        while($mostrar=mysqli_fetch_array($result)){
            ?>
            <tr>
                <td class="dato"><?php echo $mostrar['id'] ?></td>
                <td class="dato"><?php echo $mostrar['nombre'] ?></td>
                <td class="dato"><?php echo $mostrar['apeidoP'] ?></td>
                <td class="dato"><?php echo $mostrar['apeidoM'] ?></td>
                <td class="dato"><?php echo $mostrar['correo'] ?></td>
                <td class="dato"><?php echo $mostrar['fecha'] ?></td>
                <td class="dato"><?php echo $mostrar['sexo'] ?></td>
                <td class="dato"><?php echo $mostrar['edad'] ?></td>
                <?php
                $canthombres = $canthombres + 1;
                $respuestas = 0;
                $respuestas = $mostrar['respuesta1'] + $mostrar['respuesta2'] + $mostrar['respuesta3'] +$mostrar['respuesta4']
                    + $mostrar['respuesta5'] + $mostrar['respuesta6'] + $mostrar['respuesta7'] + $mostrar['respuesta8']
                    + $mostrar['respuesta9'] + $mostrar['respuesta10'];
                $hombres = $hombres + $respuestas;
                ?>
                <td class="dato"><?php echo $respuestas ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
    $mujeress = null;
    $hombress = null;
    $promedioAlto = null;
    $promedioMujeres = null;
    $promedioHombres = null;
    $cont = 0;
    $cant = 0;
    if($cantmujeres == 0){
        $mujeress = "No hay promedio de Mujeres";
    }else{
        $promedioMujeres = $mujeres / $cantmujeres;
        $mujeress = "El promedio de las Mujeres es de: ".$promedioMujeres;
        $cont = 1;
    }
    if($canthombres == 0){
        $hombress = "No hay promedio de Hombres";
    }else{
        $promedioHombres = $hombres / $canthombres;
        $hombress = "El promedio de los Hombres es de: ".$promedioHombres;
        $cant = 1;
    }
    if($cont == 1 && $cant == 1) {
        if ($promedioMujeres > $promedioHombres) {
            $promedioAlto = "Las Mujeres tiene mayor promedio de " . $promedioMujeres;
        }
        if ($promedioMujeres < $promedioHombres) {
            $promedioAlto = "Los Hombres tiene mayor promedio de " . $promedioHombres;
        }
        if ($promedioMujeres == $promedioHombres) {
            $promedioAlto = "Las Mujeres y los Hombres tienen el mismo promedio de " . $promedioMujeres;
        }
    }else if($cont == 1 && $cant == 0){
        $promedioAlto = "Las Mujeres tiene mayor promedio de " . $promedioMujeres." por que no hay Hombres ";
    }else if($cont == 0 && $cant == 1){
        $promedioAlto = "Los Hombres tiene mayor promedio de " . $promedioHombres." por que no hay Mujeres ";
    }

    ?>
    <div class="">
        <div class="col-md-10 offset-1">
            <br>
            <br>
            <h3><?php echo $mujeress?></h3>
            <h3><?php echo $hombress?></h3>
            <h3 style="color: mediumblue"><?php echo $promedioAlto?></h3>
            <br>
            <br>
        </div>
        <center>
            <button type="button" class="btn btn-primary" id="botoncancelar"><a href="Index.php?controller=Admin&action=regresarAdmin" class="nav-link" style="color: white">Regresar a la Pagina Principal</a></button>
        </center>

    </div>
</div>
</body>
<!--JS con Bootstrap-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</html>