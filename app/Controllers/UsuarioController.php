<?php
include "app/Models/Conexion.php";
include "app/Models/Usuario.php";
use Models\Conexion;
use Models\Usuario;
class UsuarioController
{
    public function home()
    {
        require_once "app/Views/index.php";
    }
    public function examen()
    {
        require_once "app/Views/examen.php";
    }
    public function agregar()
    {
        if (isset($_POST)) {
            $usuario = new \Models\Usuario();
            $usuario->nombre = $_POST["nombre"];
            $usuario->apeidoP = $_POST["apeidoP"];
            $usuario->apeidoM = $_POST["apeidoM"];
            $usuario->correo = $_POST ["correo"];
            $usuario->fecha = $_POST["fecha"];
            $usuario->sexo = $_POST["sexo"];
            $usuario->edad = $_POST["edad"];
            $usuario->respuesta1 = $_POST["respuesta1"];
            $usuario->respuesta2 = $_POST["respuesta2"];
            $usuario->respuesta3 = $_POST["respuesta3"];
            $usuario->respuesta4 = $_POST["respuesta4"];
            $usuario->respuesta5 = $_POST["respuesta5"];
            $usuario->respuesta6 = $_POST["respuesta6"];
            $usuario->respuesta7 = $_POST["respuesta7"];
            $usuario->respuesta8 = $_POST["respuesta8"];
            $usuario->respuesta9 = $_POST["respuesta9"];
            $usuario->respuesta10 = $_POST["respuesta10"];
            $usuario->insertar ();
            header('Location: /examen/Index.php?controller=Usuario&action=examen');
        }
    }

}