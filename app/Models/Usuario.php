<?php
namespace Models;
class Usuario extends Conexion
{
    //variables del examen para la base de datos
    public $id;
    public $nombre;
    public $apeidoP;
    public $apeidoM;
    public $correo;
    public $fecha;
    public $sexo;
    public $edad;
    public $respuesta1;
    public $respuesta2;
    public $respuesta3;
    public $respuesta4;
    public $respuesta5;
    public $respuesta6;
    public $respuesta7;
    public $respuesta8;
    public $respuesta9;
    public $respuesta10;

    //Funcion para insertar un registro en la base de datos
    function insertar (){
        $pre = mysqli_prepare($this->con, "INSERT INTO usuarios (nombre, apeidoP, apeidoM, correo, fecha, sexo, edad, 
respuesta1,respuesta2,respuesta3,respuesta4,respuesta5,respuesta6,respuesta7,respuesta8,respuesta9,respuesta10) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $pre -> bind_param("sssssssiiiiiiiiii", $this -> nombre, $this -> apeidoP, $this -> apeidoM, $this -> correo, $this -> fecha, $this ->sexo, $this ->edad, $this ->respuesta1, $this->respuesta2, $this->respuesta3, $this->respuesta4, $this->respuesta5, $this->respuesta6, $this->respuesta7, $this->respuesta8, $this->respuesta9, $this->respuesta10);
        $pre -> execute();
        $pre= mysqli_prepare($this->con, "SELECT LAST id");
    }
}