

$(document).ready(function() {
    var boton = document.getElementById("boton");
    boton.onclick = function(e) {
        // evitamos la acción por defecto
        e.preventDefault();
        //Se extraer=n los nombres
        var nombre1 = document.getElementById("nombre1").value;
        var nombre2 = document.getElementById("nombre2").value;
        if (nombre1 == nombre2) {
            //Si el nombre es igual se pide cambiar
            $('#validarNombres').text("No se puede comprar al mismo alumno 2 veces!").css("color", "red");
        } else {
            //Si los nombres son diferentes, lo dejara pasar a la vista de compracacion
            $('#validarNombres').text("");
            var s = document.createElement("script");
            window.location.href = 'Index.php?controller=Admin&action=comparacion&id1='+nombre1+'&id2='+nombre2;
        }
    }
});
