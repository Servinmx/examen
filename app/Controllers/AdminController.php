<?php
include "app/Models/Conexion.php";
include "app/Models/Admin.php";
use Models\Conexion;
use Models\Admin;
class AdminController
{
    public function __construct(){

    }

    //Funcion para la VISTA de la pagina para registrar datos
    public function inicio(){
        require 'app/Views/index.php';
    }
    //Funcion para la VISTA del Login
    public function login(){
        require 'app/Views/login.php';
    }
    //Funcion para iniciar secion en el LOGIN
    public function start(){
        if(isset($_POST["correo"]) && isset($_POST["contrasenia"])) {
            $correo = $_POST["correo"];
            $contrasenia = $_POST["contrasenia"];
            $verificar = Admin::verificarAdmin($correo, $contrasenia);
            if (!$verificar) {
                echo "Error, Administrador no Encontrado";
            } else {
                require 'app/Views/verRespuestas.php';
            }
        }
    }
    //Funcion para acceder al admin sin login
    public function regresarAdmin(){
        require 'app/Views/verRespuestas.php';
    }
    //Funcion para la vista compracacion
    public function comparacion()
    {
        $id1 = $_GET["id1"];
        $id2 = $_GET["id2"];
        require_once "app/Views/comparacion.php";
    }
    //Funcion para la vista promedios
    public function promedio()
    {
        $f1 = $_GET["f1"];
        $f2 = $_GET["f2"];
        require_once "app/Views/promedio.php";
    }
}