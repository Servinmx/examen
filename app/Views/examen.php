<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <!--CSS con Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Inicio</title>
    <link rel="stylesheet" href="public/css/base.css">
    <link rel="stylesheet" href="public/css/index.css">

</head>
<body background="https://static8.depositphotos.com/1033704/980/i/600/depositphotos_9808899-stock-photo-silver-gray-background.jpg">
<div class="container">
    <div class=" card">
        <div class="card-header"><center><h5 class="display-5" style="font-family: 'Bodoni MT'" >Examen(Cliente-Servidor)</h5></center></div>
        <div class="card-body">
            <form action="/examen/?controller=Usuario&action=agregar" method="POST">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="apeidoP">Apellido paterno</label>
                        <input type="text" class="form-control" id="apeidoP" name="apeidoP" placeholder="Apeido paterno">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="apeidoM">Apellido materno</label>
                        <input type="text" class="form-control" id="apeidoM" name="apeidoM" placeholder="Apeido materno">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="correo">Correo</label>
                        <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="fecha">Fecha de registro</label>
                        <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Fecha">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="edad">edad</label>
                        <input type="text" class="form-control" id="edad" name="edad" placeholder="Edad">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputSexo">Sexo</label>
                        <select id="sexo" name="sexo" class="form-control">
                            <option value="Selecciona">Selecciona</option>
                            <option value="Femenino">Femenino</option>
                            <option value="Masculino">Masculino</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-8">
                        <label class="#" for="inputGroupSelect01">1.-¿En qué lugar se ejecuta el
                            código PHP?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta1" id="respuesta1" required>
                            <option selected>Selecciona ...</option>
                            <option value=1>Servidor</option>
                            <option value=0>Cliente (ordenador propio)</option>
                            <option value=0>En ambos</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="col-md-12">
                    <div class="col-md-8">
                        <label class="#" for="inputGroupSelect01">2.-¿Cuáles de estas son marcas para
                            la inserción del código PHP en las páginas HTML?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta2" id="respuesta2"  required>
                            <option selected>Selecciona ...</option>
                            <option value=1>< ? y ? ></option>
                            <option value=0>< php >< /php >/option>
                            <option value=0><# y #></option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="col-md-12">
                    <div class="col-md-8">
                        <label class="#" for="inputGroupSelect01">3.-¿En qué atributo de un
                            formulario especificamos la página a la que se van a enviar los datos del mismo?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta3" id="respuesta3" required>
                            <option selected>Selecciona ...</option>
                            <option value=0>name</option>
                            <option value=0>file</option>
                            <option value=1>action</option>
                            <option value=0>description</option>
                        </select>
                    </div>
                </div>
                <hr>

                <div class="col-md-12">
                    <div class="col-md-8">
                        <label class="#" for="inputGroupSelect01">4.-¿Cuál de estas instrucciones
                            está correctamente escrita en PHP?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta4" id="respuesta4" required>
                            <option selected>Selecciona ...</option>
                            <option value=0>if (a=0): print a;</option>
                            <option value=1>if (a==0) echo “hola mundo”;</option>
                            <option value=0>if (a==0) { echo ok }</option>
                            <option value=0>if (a==0): print a;</option>
                        </select>
                    </div>
                </div>
                <hr>

                <div class="col-md-12">
                    <div class="col-md-10">
                        <label class="#" for="inputGroupSelect01">5.-¿Cuál de estas instrucciones PHP
                            imprimirá por pantalla correctamente el mensaje “Hola Mundo” en letra negrita?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta5" id="respuesta5" required>
                            <option selected>Selecciona ...</option>
                            <option value=0>print < strong >Hola Mundo < /strong>;</option>
                            <option value=0>print (< strong>Hola Mundo< /strong>);</option>
                            <option value=1>print ("< strong>Hola Mundo < /strong>");</option>
                        </select>
                    </div>
                </div>
                <hr>

                <div class="col-md-12">
                    <div class="col-md-10">
                        <label class="#" for="inputGroupSelect01">6.-Dos de las formas de pasar los
                            parámetros entre páginas PHP son:</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta6" id="respuesta6" required>
                            <option selected>Selecciona ...</option>
                            <option value=0>Require e Include</option>
                            <option value=0>Get y Put</option>
                            <option value=1>Post y Get</option>
                            <option value=0>Into e Include</option>
                        </select>
                    </div>
                </div>
                <hr>

                <div class="col-md-12">
                    <div class="col-md-10">
                        <label class="#" for="inputGroupSelect01">7.- ¿Cuál de estas instrucciones se
                            utiliza para realizar una consulta a una base de datos MySQL?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta7" id="respuesta7" required>
                            <option selected>Selecciona ...</option>
                            <option value=1>mysql_query</option>
                            <option value=0>mysql_access</option>
                            <option value=0>mysql_db_access</option>
                        </select>
                    </div>
                </div>
                <hr>

                <div class="col-md-12">
                    <div class="col-md-10">
                        <label class="#" for="inputGroupSelect01">8.-Un array es...</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta8" id="respuesta8" required>
                            <option selected>Selecciona ...</option>
                            <option value=0>Un conjunto de caracteres alfanuméricos</option>
                            <option value=0>Un sistema para convertir una variable de texto en un número</option>
                            <option value=1>Un conjunto de elementos</option>
                        </select>
                    </div>
                </div>
                <hr>

                <div class="col-md-12">
                    <div class="col-md-10">
                        <label class="#" for="inputGroupSelect01">9,- ¿Cómo se define una variable de
                            tipo string en PHP?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta9" id="respuesta9" required>
                            <option selected>Selecciona ...</option>
                            <option value=0>char str;</option>
                            <option value=0>string str;</option>
                            <option value=1>En PHP no se define el tipo de las variables explícitamente</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="col-md-12">
                    <div class="col-md-10">
                        <label class="#" for="inputGroupSelect01">10.-Tenemos el siguiente código:
                            $a=”10”; $b=$a + 2; ¿Cuál será el valor de $b?</label>
                    </div>
                    <div class="col-md-4">
                        <select class="custom-select-sm form-control" name="respuesta10" id="respuesta10" required>
                            <option selected>Selecciona ...</option>
                            <option value=0>"12"</option>
                            <option value=1>12</option>
                            <option value=0>"102"</option>
                            <option value=0>Ninguno (no se puede sumar un número a una cadena)</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group col-md-12 ">
                    <button class="btn btn-outline-success" id="enviar" type="submit">enviar</button>
                </div>

            </form>
            <div class="card-footer">Cualquier duda comunicate con tu profesor...</div>

        </div>
    </div>


</body>
<!--JS con Bootstrap-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script>
    $("#enviar").click(function() {
        var respuesta1 = document.getElementById("respuesta1").value;
        var respuesta2 = document.getElementById("respuesta2").value;
        var respuesta3 = document.getElementById("respuesta3").value;
        var respuesta4 = document.getElementById("respuesta4").value;
        var respuesta5 = document.getElementById("respuesta5").value;
        var respuesta6 = document.getElementById("respuesta6").value;
        var respuesta7 = document.getElementById("respuesta7").value;
        var respuesta8 = document.getElementById("respuesta8").value;
        var respuesta9 = document.getElementById("respuesta9").value;
        var respuesta10 = document.getElementById("respuesta10").value;
        var resultado = parseInt(respuesta1, 10) + parseInt(respuesta2, 10) + parseInt(respuesta3, 10) +
            parseInt(respuesta4, 10) + parseInt(respuesta5, 10) + parseInt(respuesta6, 10) + parseInt(respuesta7, 10) +
            parseInt(respuesta8, 10) + parseInt(respuesta9, 10) + parseInt(respuesta10, 10);
        alert("Tu resultado es:" + resultado);
    });
</script>
</html>