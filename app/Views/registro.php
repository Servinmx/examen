<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registrar</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>
<body background="https://image.jimcdn.com/app/cms/image/transf/none/path/s1835fc4dc8c6343c/backgroundarea/i9ff229ed6a2b6856/version/1513185954/image.jpg">
<center><h5 class="display-4" style="font-family: 'Bodoni MT'" >Registrar</h5></center>
<hr>

<div class="container regis">
    <form action="/examen/?controller=Usuario&action=agregarInfo" enctype="multipart/form-data" method="POST" id="registro">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
            </div>
            <div class="form-group col-md-4">
                <label for="apeidoP">Apellido paterno</label>
                <input type="text" class="form-control" id="apeidoP" name="apeidoP" placeholder="Apeido paterno">
            </div>
            <div class="form-group col-md-4">
                <label for="apeidoM">Apellido materno</label>
                <input type="text" class="form-control" id="apeidoM" name="apeidoM" placeholder="Apeido materno">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="correo">Correo</label>
                <input type="text" class="form-control" id="correo" name="correo" placeholder="Correo">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="fecha">Fecha de registro</label>
                <input type="text" class="form-control" id="fecha" name="fecha" placeholder="Fecha">
            </div>
            <div class="form-group col-md-4">
                <label for="edad">edad</label>
                <input type="text" class="form-control" id="edad" name="edad" placeholder="Edad">
            </div>
            <div class="form-group col-md-4">
                <label for="inputSexo">Sexo</label>
                <select id="sexo" name="sexo" class="form-control">
                    <option value="Selecciona">Selecciona</option>
                    <option value="Femenino">Femenino</option>
                    <option value="Masculino">Masculino</option>
                </select>
            </div>
        </div>
        <hr>
        <div class="form-row">
            <div class="form-group col-md-4 offset-md-5">
                <button class="btn btn-outline-primary" type="submit">Agregar</button>
            </div>
        </div>
    </form>
    <!---<?php
    foreach($Usuarios as $usuario){
        echo "<div class='card forma' style='margin: 10px'>".
            "<div class='card-body'>".
            "<h5 class='card-title'>".$usuario->id.". ".$usuario->nombre." ".$usuario->cuatrimestre."</h5>".
            "<a href='/Pruebas/?controller=Usuario&action=all&id=". $usuario->id."' class='btn btn-outline-success'>Editar</a>".
            "<form action='/Pruebas/?controller=Usuario&action=eliminar' class='card-text' enctype='multipart/form-data' method='post' id='formEliminar'>".
            "<input id='id' name='id' type='hidden' value='{$usuario -> id}'><br>".
            "<button class='btn btn-outline-danger btnEliminar' type='submit'>Eliminar</button>".
            "</form>".
            "</div></div>";
    }
    ?>
    <?php
    require_once "Views/Modal/EjemploModal.php"
    ?>--->

</div>

</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</html>