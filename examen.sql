-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2021 a las 01:27:48
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE `administradores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `apaterno` varchar(12) NOT NULL,
  `amaterno` varchar(12) NOT NULL,
  `correo` varchar(40) NOT NULL,
  `contrasenia` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`id`, `nombre`, `apaterno`, `amaterno`, `correo`, `contrasenia`) VALUES
(1, 'Johan', 'Guzman ', 'Perez', 'johanguzmpe@gmail.com', 'hola123'),
(2, 'Johana Lizbeth', 'Vizcarra', 'Vazquez', 'johanavizcarra48@gmail.com', '1234'),
(3, 'Joshua Erik', 'Torres', 'Servin', 'joshua@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apeidoP` varchar(30) NOT NULL,
  `apeidoM` varchar(30) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `fecha` date NOT NULL,
  `sexo` varchar(15) NOT NULL,
  `edad` varchar(2) NOT NULL,
  `respuesta1` int(1) NOT NULL,
  `respuesta2` int(1) NOT NULL,
  `respuesta3` int(1) NOT NULL,
  `respuesta4` int(1) NOT NULL,
  `respuesta5` int(1) NOT NULL,
  `respuesta6` int(1) NOT NULL,
  `respuesta7` int(1) NOT NULL,
  `respuesta8` int(1) NOT NULL,
  `respuesta9` int(1) NOT NULL,
  `respuesta10` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apeidoP`, `apeidoM`, `correo`, `fecha`, `sexo`, `edad`, `respuesta1`, `respuesta2`, `respuesta3`, `respuesta4`, `respuesta5`, `respuesta6`, `respuesta7`, `respuesta8`, `respuesta9`, `respuesta10`) VALUES
(3, 'Johan', 'Guzman', 'Perez', 'johanguzmpe@gmail.com', '2021-02-21', 'Masculino', '19', 0, 0, 0, 1, 0, 1, 1, 0, 0, 0),
(4, 'Joshua', 'Torres', 'Servin', 'holacomoestas@gmail.com', '2021-02-22', 'Masculino', '20', 0, 1, 0, 1, 0, 0, 0, 0, 0, 1),
(5, 'Johana', 'Vizcarra', 'Vazquez', 'johanavizcarra12@gmail.com', '2021-02-23', 'Femenino', '15', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(6, 'Andrea', 'Juarez', 'Rangel', 'brayan458@gmail.com', '2021-03-01', 'Femenino', '20', 1, 0, 1, 1, 0, 1, 0, 0, 1, 1),
(7, 'Alejandro', 'Vizcarra', 'Servin', 'esqueleto144@gmail.com', '2021-03-08', 'Masculino', '22', 1, 0, 1, 1, 0, 0, 0, 0, 0, 0),
(8, 'Carlos', 'Jimenez', 'Vazquez', 'brayan458@gmail.com', '2021-02-10', 'Masculino', '24', 0, 0, 0, 0, 0, 0, 1, 0, 0, 0),
(10, 'Andres', 'Hernandez', 'Mesa', 'hernandez15@gmail.com', '2021-02-24', 'Masculino', '23', 0, 1, 0, 1, 0, 0, 0, 0, 0, 1),
(11, 'Elizabeth', 'Perez', 'Marquez', 'elizabeth48@gmail.com', '2021-02-21', 'Femenino', '25', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores`
--
ALTER TABLE `administradores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
