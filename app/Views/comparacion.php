<?php
    $conexion=mysqli_connect('localhost', 'root', '', 'examen');
    $id1 = $_GET["id1"];
    $id2 = $_GET["id2"];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Comparacion</title>
    <link rel="stylesheet" href="public/css/login.css">
</head>
<body >
<div class="container login-container">
    <h2>Compracion de Resultados</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Aciertos</th>
        </tr>
        </thead>
        <tbody>
        <tbody>
        <?php
        $sql="SELECT * FROM `usuarios` WHERE id = $id1";
        $result=mysqli_query($conexion, $sql);
        while($mostrar=mysqli_fetch_array($result)){
            ?>
            <tr>
                <td class="dato"><?php echo $mostrar['nombre']." ".$mostrar['apeidoP']." ".$mostrar['apeidoM']?></td>
                <?php
                $nombre1 = $mostrar['nombre']." ".$mostrar['apeidoP']." ".$mostrar['apeidoM'];
                $respuesta1 = null;
                $respuesta1 = $mostrar['respuesta1'] + $mostrar['respuesta2'] + $mostrar['respuesta3'] +$mostrar['respuesta4']
                    + $mostrar['respuesta5'] + $mostrar['respuesta6'] + $mostrar['respuesta7'] + $mostrar['respuesta8']
                    + $mostrar['respuesta9'] + $mostrar['respuesta10'];
                ?>
                <td class="dato"><?php echo $respuesta1 ?></td>
            </tr>
            <?php
        }
        ?>
        <?php
        $sql="SELECT * FROM `usuarios` WHERE id = $id2";
        $result=mysqli_query($conexion, $sql);
        while($mostrar=mysqli_fetch_array($result)){
            ?>
            <tr>
                <td class="dato"><?php echo $mostrar['nombre']." ".$mostrar['apeidoP']." ".$mostrar['apeidoM']?></td>
                <?php
                $nombre2 = $mostrar['nombre']." ".$mostrar['apeidoP']." ".$mostrar['apeidoM'];
                $respuesta2 = null;
                $respuesta2 = $mostrar['respuesta1'] + $mostrar['respuesta2'] + $mostrar['respuesta3'] +$mostrar['respuesta4']
                    + $mostrar['respuesta5'] + $mostrar['respuesta6'] + $mostrar['respuesta7'] + $mostrar['respuesta8']
                    + $mostrar['respuesta9'] + $mostrar['respuesta10'];
                ?>
                <td class="dato"><?php echo $respuesta2 ?></td>
            </tr>
            <?php
            if($respuesta1 > $respuesta2){
                $respuestaalta = "El resultado mas alto es el de ".$nombre1." con ".$respuesta1." aciertos";
            }else if($respuesta1 < $respuesta2) {
                $respuestaalta = "El resultado mas alto es el de ".$nombre2." con ".$respuesta2." aciertos";
            }else if($respuesta1 == $respuesta2){
                $respuestaalta = "El resultado entre ambos es igual con ".$respuesta1." aciertos";
            }
        }
        ?>
        </tbody>
    </table>
    <div class="">
        <br>
        <br>
        <div class="col-md-10 offset-1">
            <h3 style="color: mediumblue"><?php echo $respuestaalta?></h3>
        </div>
        <center>
            <button type="button" class="btn btn-primary" id="botoncancelar"><a href="Index.php?controller=Admin&action=regresarAdmin" class="nav-link" style="color: white">Regresar a la Pagina Principal</a></button>
        </center>
    </div>
</div>
</body>
<!--JS con Bootstrap-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
</html>